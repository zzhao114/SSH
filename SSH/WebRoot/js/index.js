

  function none(){
	  var divs = document.getElementsByTagName("div");
	  for(var i=0;i<divs.length;i=i+1){
		  divs[i].style.display='none';
	  }
  }
  
  function onload(Info,info){
	none();
    if(info!=null){
		var select2=document.getElementById("select2"); 
		for(var i=0;i<select2.options.length;i=i+1){
			if(select2.options[i].value==info){
				select2.options[i].selected=true;
				break;
			}
		}
    }
   
    switch(Info){
    	case "result_add":                    		 //反馈add添加成功
    		div_add.style.display='block';
    		alert("添加成功!");
    		form_radio.select.value="user_add";  break;
    	case "result_add_fail":              		 //反馈add添加失败
    		div_add.style.display='block';
    		alert("添加失败!");
    		form_radio.select.value="user_add";  break;
    	case "result_query_user":                    //反馈user查询结果
			div_query_result_user.style.display='block';
			form_radio.select.value="query_user";break;
    	case "result_query_fail_user":				 //反馈user查询结果失败
			div_query_user.style.display='block';
			form_radio.select.value="query_user";
			alert("User不存在！");				 break;
	    case "result_query_pro":                     //反馈province查询结果
			div_query_result_o.style.display='block';
			form_radio.select.value="query_name";break;
	    case "result_query_fail_pro":				 //反馈province查询结果失败
			div_query_name.style.display='block';
			form_radio.select.value="query_name";
			alert("Province不存在！");			 break;
	    case "result_query_idcard":                  //反馈idcard查询结果
			div_query_result_o.style.display='block';
			form_radio.select.value="query_id";  break;
	    case "result_query_fail_idcard":			 //反馈idcard查询结果失败
			div_query_id.style.display='block';
			form_radio.select.value="query_id";
			alert("Idcard不存在！");				 break;
		case "result_query_all":                     //反馈所有查询结果
			div_query_result.style.display='block';
			form_radio.select.value="query_all"; break;
		case "result_query_*":						 //反馈所有查询结果失败
			alert("信息不存在！");
			div_add.style.display='block';
			form_radio.select.value="user_add";  break;
		case "result_query_role":                    //反馈Role查询结果
			div_query_result_role.style.display='block';
			form_radio.select.value="query_role";  break;
	    case "result_query_fail_role":			    //反馈Role查询结果失败
			div_query_role.style.display='block';
			form_radio.select.value="query_role";
			alert("Role不存在！");				 break;
		default:
			div_add.style.display='block';
			
    }
 }	
  
	function Add(){
		var objExp=/[\u4E00-\u9FA5]{2,}/;
		if(form_add.username.value==""){
			alert("请输入用户名！");form_add.username.focus();return ;
		}else if(objExp.test(form_add.username.value)==false){
			alert("请输入中文用户名！");return;
		}else if(form_add.password.value==""){
			alert("请输入密码！");form_add.password.focus();return ;
		}else if(form_add.password.value!=form_add.repassword.value){
			alert("输入密码不一致！");form_add.password.focus();return ;
		}else {
			form_add.submit();
		}
	}
	
	function Query(){
		if(form_query_id.idcardCode.value==""){
			form_query_id.submit();
		}else if(isNaN(form_query_id.idcardCode.value)==true){
			alert("请输入数字！");form_query_id.idcardCode.focus();return ;
		}else form_query_id.submit();
	}
	
	function back(){
		window.history.back(-1);
	}
	
	function RELODE(){
		location.reload(false) ;
	}
	
	function radio_add(){
		none();
		div_add.style.display='block';
	}
	
	function radio_query_all(){
		form_radio.method="post";
		form_radio.action="userAction!query_all";
		form_radio.submit();
	} 
	
	function select2(){
		var asc=document.getElementById("select2").value;
		window.location.href="userAction!query_all?asc="+asc;
	}
	
	function radio_query_id(){
		none();
		div_query_id.style.display='block';
	}
	
	function radio_query_name(){
		none();
		div_query_name.style.display='block';
	}
	
	function radio_query_user(){
		none();
		div_query_user.style.display='block';
	}
	
	function radio_query_role(){
		none();
		div_query_role.style.display='block';
	}
