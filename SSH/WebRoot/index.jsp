<%@ page language="java" import="java.util.*" pageEncoding="gbk"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@page import="com.VO.* "%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>zzhao</title>
    <script type="text/javascript" src="js/index.js"></script>
    <link type="text/css" rel="stylesheet" href="css/index.css" /> 
    <link rel="shortcut icon" href="./img/icon/ali.gif" type="image/gif"/>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
 <body onload="onload('<%=request.getAttribute("info")%>','<%=request.getAttribute("asc")%>')">
 	<form  name="form_radio" >
 	  <table width="100%" align="center">
 		<tr>
 			<td align="left" width="60%">
 				<input type=radio name=select value=user_add onclick="radio_add()" >信息添加
 				<input type=radio name=select value=query_user onclick="radio_query_user()">User查询
				<input type=radio name=select value=query_id onclick="radio_query_id()"> IDcard查询
  				<input type=radio name=select value=query_name onclick="radio_query_name()"> Province查询
  				<input type=radio name=select value=query_role onclick="radio_query_role()"> Role查询
 				<input type=radio name=select value=query_all onclick="radio_query_all()">所有信息
 			</td>
 			<td align="right" width="35%"><a href="#">作者</a> | <a href="#">张曌</a>
 			</td>
 			<td align="right" width="5%"></td>
 		</tr>
 	</table>
 </form>
 
  <div id=div_add style=display:block>
     <form name="form_add" action ="userAction!add" method="post" target="_self" ;>
    	<table align="center" width="500">
    		<tr>
    			<td align="center"colspan="2">
    				<h2>添加用户信息</h2><hr>
    			</td>
    		</tr>
    		<tr>
    			<td align="right" width="150">用 户 名 :</td>
    			<td>&nbsp;&nbsp;<input type="text"name="username" style="width:170px"/></td>
    		</tr>
    		<tr>
    			<td align="right">密      码 :</td>
    			<td>&nbsp;&nbsp;<input type="password" name="password" style="width:170px"/></td>
    		</tr>
    			<tr>
    			<td align="right">确 认 密 码 :</td>
    			<td>&nbsp;&nbsp;<input type="password" name="repassword" style="width:170px"/></td>
    		</tr>
    		<tr>
    			<td align="right">身 份 证 号 :</td>
    			<td>&nbsp;&nbsp;<input type="text" name="idcardCode" style="width:170px"/></td>
    		</tr>
    		<tr>
    			<td align="right">所 在 省 :</td>
    			<td>&nbsp;&nbsp;<input type="text" name="pro_name" style="width:170px"/></td>
    		</tr>
    		<tr>
    			<td align="right">权 限  :</td>
    			<td>
    				<input type="checkbox" name="rolename" value="none"/> None
    				<input type="checkbox" name="rolename" value="save-update"/> Save-Update
    				<input type="checkbox" name="rolename" value="delete"/> Delete
    				<input type="checkbox" name="rolename" value="all"/> All
    			</td>
    		</tr>
    		<tr>
    			<td align="right"> </td>
    			<td align="center"colspan="1">
    				<input type ="button"value="返 回"onclick="back()">&nbsp;
    				<input type ="button"value="添 加"onclick="Add()">&nbsp;
    				<input type ="reset"value="重 置">&nbsp;
  				</td>
    		</tr><tr></tr>
    	</table>
    </form>
   </div>
 <div id=div_query_name style=display:none>
 <form name="form_query_name" action ="provinceAction!Query" method="post" target="_self" ;>
   <table align="center"width="500">
    		<tr>
    			<td align="center"colspan="2">
    				<h2>Province查询</h2>
    				(一对多双向关联 || 值空时查询所有Province)<hr>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">Province ：</td>
    			<td><input type="text"name="pro_name" maxlength="6" /></td>
    		</tr>
    		<tr>
    			<td align="right"> </td>
    			<td align="center"colspan="1">&nbsp; 
    				<input type ="submit"value="查 询">
    				<input type ="reset"value="重 置">
  				</td>
    		  </tr>
    		
    </table> 
   </form>
</div>
<div id=div_query_id style=display:none>
 <form name="form_query_id" action ="idcardAction!Query" method="post" target="_self" ;>
   <table align="center"width="500">
    		<tr>
    			<td align="center"colspan="2">
    				<h2>IDcard查询</h2>
    				(一对一主键双向关联 || 值空时查询所有IDcard)<hr>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">IDcard ：</td>
    			<td><input type="text"name="idcardCode" maxlength="6" /></td>
    		</tr>
    		<tr>
    			<td align="right"> </td>
    			<td align="center"colspan="1">&nbsp; 
    				<input type ="button"value="查 询"onclick="Query()">
    				<input type ="reset"value="重 置">
  				</td>
    		  </tr>
    		
    </table> 
   </form>
</div> 
<div id=div_query_user style=display:none>
 <form name="form_query_user" action ="userAction!query_name" method="post" target="_self" ;>
   <table align="center"width="500">
    		<tr>
    			<td align="center"colspan="2">
    				<h2>User查询</h2>
    				(一对一主键 且一对多双向关联 || 值空时查询所有User)<hr>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">UserName ：</td>
    			<td><input type="text"name="username" maxlength="6" /></td>
    		</tr>
    		<tr>
    			<td align="right"> </td>
    			<td align="center"colspan="1">&nbsp; 
    				<input type ="submit"value="查 询">
    				<input type ="reset"value="重 置">
  				</td>
    		  </tr>
    		
    </table> 
   </form>
</div>   
<div id=div_query_role style=display:none>
 <form name="form_query_user" action ="roleAction!Query" method="post" target="_self" ;>
   <table align="center"width="500">
    		<tr>
    			<td align="center"colspan="2">
    				<h2>Role查询</h2>
    				(多对多关联 || 值空时查询所有Role)<hr>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">RoleName ：</td>
    			<td><input type="text"name="rolename" /></td>
    		</tr>
    		<tr>
    			<td align="right"> </td>
    			<td align="center"colspan="1">&nbsp; 
    				<input type ="submit"value="查 询">
    				<input type ="reset"value="重 置">
  				</td>
    		  </tr>		
   		 </table> 
   </form>
</div>  
 <div id=div_query_result style=display:none>
 
   <table id="table_query_result" align="center"  border="1"  bordercolor="black"  cellspacing="0" >
    <tr>
		<td align="center" colspan="6">
			<span style="float:right">
				<select name="select2" id="select2" onchange="select2()">
 					<option value="DESC">降序查询</option>
 					<option value="ASC">升序查询</option>
 				</select>
 			</span>
			<h2>所有信息</h2>	
		</td>
	</tr>
	<tr align="center" bgcolor="#e1ffc1" >
		<td width="75"><b>ID</b></td>
		<td width="75"><b>用户名</b></td>
		<td width="100"><b>身份证号</b></td>
		<td width="75"><b>所在省</b></td>
		<td width="175"><b>权限</b></td>
		<td width="75"><b>密码</b></td>
	</tr>
	<%
		if(request.getAttribute("list_user")!=null){
			List<User> list = (List<User>)request.getAttribute("list_user");	
			for(User u : list){
	%>
	<tr align="center" bgcolor="white">
		<td><%=u.getUserid() %></td>
		<td><%=u.getUsername()%></td>
		<td><%=u.getIdcard().getIdcardCode()%></td>
		<td>
			<% if(u.getProvince()!=null){ %>
				<%=u.getProvince().getPro_name()%>
			<% } %>
		</td>
		<td>
		<%
				Set<Role> roles=u.getRoles();
				if(roles.size()!=0){
					Iterator<Role> it=roles.iterator();
					for(Role role:roles){
						it.next();
			 %>
			 <%=role.getRolename() %><% if(it.hasNext()){ %> | <%} %>
	    <%}} %>
		</td>
		<td><%=u.getpassword()%></td>
	</tr>
	<%	
			}	
		}
	%>	
	<tr>
		<td align="center" colspan="5" bgcolor="white">	
			<%=request.getAttribute("bar")%>
		</td>
		<td align="center" colspan="1" bgcolor="white">
			<a href="javascript:back()">返回</a>
			</td>
	</tr>
  </table>

 </div>      
<div id=div_query_result_o style=display:none>
    <table align="center"  bordercolor="black" bgcolor="white"  cellspacing="0" border="1">
	<% 
        String s2="",s1=""; 
     	if(request.getAttribute("list_pro")!=null){
  			s2="Province信息";
  			s1="Province";
  		}else {	s2="IDcard信息";s1="IDcard";}
  	%>
	<tr>
		<td align="center" colspan="6" bgcolor="white">
			<h2><%=s2 %></h2>
		</td>
	</tr>
	<tr align="center" bgcolor="#e1ffc1" >
		<td width="75"><b>ID</b></td>
		<td width="75"><b><%=s1 %></b></td>
		<td width="150" colspan="2"><b>信息修改</b></td>
		<td width="150" colspan="2"><b>操  作</b></td>
	</tr>
  	 <% 	if(request.getAttribute("list_idcard")!=null){
			List<Idcard> list = (List<Idcard>)request.getAttribute("list_idcard");	
			for(Idcard u : list){
	%>
	<tr align="center" bgcolor="white">
		<td><%=u.getId()%></td>
		<td><%=u.getIdcardCode()%></td>
		<td colspan="2">
			<form name="form_update" action ="idcardAction!Update" method="post" style="padding: 0px;margin: 0px;" onsubmit="return check(this);">
		   	  <input type="hidden" name="id" value="<%=u.getId()%>">
		   	  <input type="text" name="idcardCode" size="15" maxlength="6" >
		      <input type ="submit" value="确  定">
			</form>
        </td> 
		<td>
			<a href="idcardAction!Delete?id=<%=u.getId()%> ">删除</a>
	    </td>
	    <td>
			<a href="idcardAction!Query?idcardCode=<%=u.getIdcardCode()%> ">查看</a>
	    </td>
	</tr>
	<%		
		if(list.size()==1){
	%>
		<tr>
			<td align="left" colspan="6">对应用户信息：</td>
		</tr>
		<tr align="center">
			<td><b>用户名</b></td>
			<td><b>IDcard</b></td>
			<td><b>权限</b></td>
			<td><b>Province</b></td>
			<td><b>密码</b></td>
			<td></td>
		</tr>
		<tr align="center">
			<td><%=u.getUser().getUsername()%></td>
			<td><%=u.getIdcardCode()%></td>
			<td>
			<%
				Set<Role> roles=u.getUser().getRoles();
				if(roles.size()!=0){
					Iterator<Role> it=roles.iterator();
					for(Role role:roles){
						it.next();
			 %>
			 <%=role.getRolename() %><% if(it.hasNext()){ %> | <%} %>
			 <%}} %>
			</td>
			<td>
				<%if(u.getUser().getProvince()!=null){ %>
					<%=u.getUser().getProvince().getPro_name()%>
				<%} %>
			</td>
			<td><%=u.getUser().getpassword()%></td>
			<td></td>
		</tr>					
	<% 
		}}}
	%>	
    <% 	if(request.getAttribute("list_pro")!=null){
			List<Province> list = (List<Province>)request.getAttribute("list_pro");	
			for(Province u: list){
	%>
	<tr align="center" bgcolor="white">
		<td><%=u.getId()%></td>
		<td><%=u.getPro_name()%></td>
		
		<td colspan="2">
			<form name="form_update" action ="provinceAction!Update" method="post" style="padding: 0px;margin: 0px;" onsubmit="return check(this);">
		   	  <input type="hidden" name="id" value="<%=u.getId()%>">
		   	  <input type="text" name="pro_name" size="15" maxlength="6" >
		      <input type ="submit" value="确  定">
			</form>
        </td> 
		<td width="75">
			<a href="provinceAction!Delete?id=<%=u.getId()%>">删除</a>
	    </td>
	    <td width="75"><a href="provinceAction!Query?pro_name=<%=u.getPro_name()%>">查看</a></td>
	</tr>
	<%		
		if(list.size()==1){
	%>
		<tr>
			<td align="left" colspan="6">包含用户信息：</td>
		</tr>
		<tr align="center">
			<td><b>用户名</b></td>
			<td><b>IDcard</b></td>
			<td><b>权限</b></td>
			<td><b>Province</b></td>
			<td><b>密码</b></td>
			<td><b>操作</b></td>
		</tr>
	<%
		Set<User> users=u.getUsers();
		if(users.size()==0){
	%>
	<tr align="center"><td colspan="6">无用户信息！</td></tr>
	<%  }
		for(User user:users){
	 %>
		<tr align="center">
			<td><%=user.getUsername()%></td>
			<td><%=user.getIdcard().getIdcardCode()%></td>
			<td>
			<%
				Set<Role> roles=user.getRoles();
				if(roles.size()!=0){
					Iterator<Role> it=roles.iterator();
					for(Role role:roles){
						it.next();
			 %>
			 <%=role.getRolename() %><% if(it.hasNext()){ %> | <%} %>
			 <%}} %>
			</td>
			<td><%=user.getProvince().getPro_name()%></td>
			<td><%=user.getpassword()%></td>
			<td><a href="provinceAction!Delete_user?id=<%=user.getUserid() %>& pro_name=<%=u.getPro_name() %>">删除</a></td>
		</tr>					
	<% 
		}}}}
	%>		

	<tr align="center">
		<td colspan="5" ></td>
		<td colspan="1">
			<a href="javascript:back()">返回</a>
		</td>
	</tr>
  </table>
</div> 
<div id=div_query_result_user style=display:none>
<form name="form_update" action ="userAction!update" method="post" style="padding: 0px;margin: 0px;" onsubmit="return check(this);">
<table id="table_query_result" align="center"  border="1"  bordercolor="black"  cellspacing="0" >
    <tr>
		<td align="center" colspan="6">
			<h2>User信息</h2>	
		</td>
	</tr>
	<tr align="center" bgcolor="#e1ffc1" >
		<td width="75"><b>ID</b></td>
		<td width="150" colspan="2"><b>用户名</b></td>
		<td width="140"><b>密码</b></td>
		<td width="150" colspan="2"><b>操  作</b></td>
	</tr>
	<%
		if(request.getAttribute("list_user")!=null){
			List<User> list = (List<User>)request.getAttribute("list_user");	
			for(User u : list){
	%>
	<tr align="center" bgcolor="white">
		<td><%=u.getUserid()%></td>
		<td colspan="2"><%=u.getUsername()%></td>
		<td><%=u.getpassword()%></td>
		<td>
			<a href="userAction!delete?userid=<%=u.getUserid()%>">删除</a>
	    </td>
	    <td>
			<a href="userAction!query_id?userid=<%=u.getUserid()%>">查看</a>
	    </td>
	</tr>
	<%		
		if(list.size()==1){
	%>
		<tr>
			<td align="left" colspan="6">对应用户信息：</td>
		</tr>
		<tr align="center">
			<td><b>用户名</b></td>
			<td><b>IDcard</b></td>
			<td><b>Province</b></td>
			<td><b>权限</b></td>
			<td><b>密码</b></td>
			<td><b>操作</b></td>
		</tr>
		<tr align="center">
			<td><%=u.getUsername()%></td>
			<td><%=u.getIdcard().getIdcardCode()%></td>
			<td><%if(u.getProvince()!=null){%><%=u.getProvince().getPro_name()%><%} %></td>
			<td>
			<%
				Set<Role> roles=u.getRoles();
				if(roles.size()!=0){
					Iterator<Role> it=roles.iterator();
					for(Role role:roles){
						it.next();
			 %>
			 <%=role.getRolename() %><% if(it.hasNext()){ %> | <%} %>
			 <%}} %>
			</td>
			<td><%=u.getpassword()%></td>
			<td><b><input type ="submit" value="确  定"></b></td>
		</tr>
		<tr>
			<td align="center">密码修改</td>
			<td colspan="4">
		   		<input type="hidden" name="userid" value="<%=u.getUserid()%>">
		   	 	<input type="hidden" name="username" value="<%=u.getUsername()%>">
		   	 	&nbsp;&nbsp;<input type="text" name="password">
       		</td>
       		<td></td>
        </tr>
		<tr>
			<td colspan="1" align="center">省份修改</td>
			<td colspan="4">
				&nbsp;&nbsp;<input type="text" name="pro_name">
			</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="1" align="center">权限修改</td>
			<td colspan="4">
				<input type="checkbox" name="rolename" value="none"/> None
    			<input type="checkbox" name="rolename" value="save-update"/> Save-Update
    			<input type="checkbox" name="rolename" value="delete"/> Delete
    			<input type="checkbox" name="rolename" value="all"/> All
    		</td>
    		<td></td>
    	</tr>					
	<% 
		}}}
	%>
	<tr>
		<td align="center" colspan="5"></td>
		<td align="center" colspan="1">
			<a href="javascript:back()">返回</a>
			</td>
	</tr>
  </table>
</form>
</div>
<div id=div_query_result_role style=display:none>
<table id="table_query_result_rale" align="center"  border="1"  bordercolor="black"  cellspacing="0" >
    <tr>
		<td align="center" colspan="6">
			<h2>Role信息</h2>	
		</td>
	</tr>
	<tr align="center" bgcolor="#e1ffc1" >
		<td width="75" colspan="1"><b>ID</b></td>
		<td colspan="2" width="140"><b>权限</b></td>
		<td colspan="1" width="200"><b>信息修改</b></td>
		<td colspan="2" width="150"><b>操作</b></td>
	
	</tr>
	<%
		if(request.getAttribute("list_role")!=null){
			List<Role> list = (List<Role>)request.getAttribute("list_role");	
			for(Role u : list){
	%>
	<tr align="center" bgcolor="white">
		<td colspan="1" width="75"><%=u.getId()%></td>
		<td colspan="2" width="140"><%=u.getRolename()%></td>
		<td colspan="1">
			<form name="form_update" action ="roleAction!Update" method="post" style="padding: 0px;margin: 0px;" onsubmit="return check(this);">
		   	  <input type="hidden" name="id" value="<%=u.getId()%>">
		   	  <input type="text" name="rolename" size="15" maxlength="6" >
		      <input type ="submit" value="确  定">
			</form>
        </td>
        <td><a href="roleAction!Delete_role?id=<%=u.getId() %>">删除</a></td> 
        <td><a href="roleAction!Query?rolename=<%=u.getRolename()%>">查看</a></td> 
	</tr>
	<%		
		if(list.size()==1){
	%>
		<tr>
			<td align="left" colspan="6">拥有此权限用户信息：</td>
		</tr>
		<tr align="center">
			<td width="75"><b>用户名</b></td>
			<td width="75"><b>IDcard</b></td>
			<td width="75"><b>Province</b></td>
			<td width="140"><b>权限</b></td>
			<td width="75"><b>密码</b></td>
			<td width="75" colspan="1"><b>操作</b></td>
		</tr>
	<%
		Set<User> users=u.getUsers();
		if(users.size()==0){
	%>
	<tr align="center"><td colspan="6">无用户信息！</td></tr>
	<%  }
		for(User user:users){
	 %>
		<tr align="center">
			<td><%=user.getUsername()%></td>
			<td><%=user.getIdcard().getIdcardCode()%></td>
			<td><%if(user.getProvince()!=null){%><%=user.getProvince().getPro_name()%><%} %></td>
			<td>
			<%
				Set<Role> roles=user.getRoles();
				if(roles.size()!=0){
					Iterator<Role> it=roles.iterator();
					for(Role role:roles){
						it.next();
			 %>
			 <%=role.getRolename() %><% if(it.hasNext()){ %> | <%} %>
			 <%}} %>
			</td>
			<td><%=user.getpassword()%></td>
			<td><a href="roleAction!Delete?userid=<%=user.getUserid()%>&id=<%=u.getId()%>&rolename=<%=u.getRolename()%>">删除</a></td>
		</tr>				
	<% 
		}}}}
	%>
		<tr>
			<td colspan="5"></td>
			<td align="center" colspan="1">
				<a href="javascript:back()">返回</a>
			</td>
		</tr>
  </table>

</div>

 </body>

</html>
