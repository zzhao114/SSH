package com.DAO;

import java.util.List;
import com.BaseDAO.BaseDAO;
import com.VO.Idcard;

public interface IdcardDAO extends BaseDAO<Idcard>{

	public List<Idcard> queryByName(int idcardCode) throws Exception;

}
