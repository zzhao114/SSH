package com.DAO;

import java.util.List;
import com.BaseDAO.BaseDAO;
import com.VO.Role;

public interface RoleDAO extends BaseDAO<Role>{

	public void delete_role(int id) throws Exception;

	public void delete(int userid, int roleid) throws Exception;

	public List<Role> queryByName(String rolename) throws Exception;
}
