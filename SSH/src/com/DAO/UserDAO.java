package com.DAO;

import java.util.*;

import com.BaseDAO.BaseDAO;

import com.VO.*;

public interface UserDAO extends BaseDAO<User>{
	
	public String insert(User user, Idcard idcaed) throws Exception;

	public List<User> queryByName(String username) throws Exception;

	public List<User> find(int page, String asc) throws Exception;

	public int findCount() throws Exception;

	public String bar(int page, int count, String action) throws Exception; // ��ҳ��
}
