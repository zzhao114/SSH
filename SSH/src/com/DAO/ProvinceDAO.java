package com.DAO;

import java.util.List;

import com.BaseDAO.BaseDAO;

import com.VO.Province;

public interface ProvinceDAO extends BaseDAO<Province>{
	
	public List<Province> queryByName(String pro_name) throws Exception;
}
