package com.DAOImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;
import com.BaseDAO.BaseDAOImpl;
import com.DAO.IdcardDAO;
import com.SessionFactory.HibernateSession;
import com.VO.Idcard;
import com.VO.Role;
import com.VO.User;

public class IdcardDAOImpl extends BaseDAOImpl<Idcard> implements IdcardDAO {
	Session session = null;

	public void delete(int id) throws Exception {
		try {
			session = HibernateSession.getSession();
			Idcard idcard = (Idcard) session.get(Idcard.class, new Integer(id));
			User user = idcard.getUser();
			if (user.getProvince() != null) {
				user.getProvince().getUsers().remove(user);
				user.setProvince(null);
			}
			if (user.getRoles() != null) {
				Set<Role> roles = user.getRoles();
				for (Role role : roles) {
					user.setRoles(new HashSet<Role>());
					role.setUsers(new HashSet<User>(0));
					user.getRoles().remove(role);
					role.getUsers().remove(user);
				}
			}
			session.delete(idcard);
			session.flush();
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

	}

	public List<Idcard> queryByName(int idcardCode) throws Exception {
		List<Idcard> list = new ArrayList<Idcard>();
		list = null;
		try {
			session = HibernateSession.getSession();
			String hql = "from Idcard idcard where idcard.idcardCode=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, idcardCode);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

		return list;
	}

	public void update(Idcard idcard) throws Exception {
		try {
			session = HibernateSession.getSession();
			Idcard idcard_update = (Idcard) session.get(Idcard.class,
					new Integer(idcard.getId()));
			idcard_update.setIdcardCode(idcard.getIdcardCode());
			session.flush();
		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

	}

}
