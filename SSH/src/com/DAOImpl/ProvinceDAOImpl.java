package com.DAOImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;
import com.BaseDAO.BaseDAOImpl;
import com.DAO.ProvinceDAO;
import com.SessionFactory.HibernateSession;
import com.VO.Province;
import com.VO.User;

public class ProvinceDAOImpl extends BaseDAOImpl<Province> implements ProvinceDAO {

	Session session = null;

	public void delete(int id) throws Exception {
		try {
			session = HibernateSession.getSession();
			Province province = (Province) session.get(Province.class,
					new Integer(id));
			Set<User> users = province.getUsers();
			if (province.getUsers() != null) {
				for (User user : users) {
					user.getProvince().setUsers(new HashSet<User>());
					user.setProvince(null);
					province.getUsers().remove(user);
				}
			}
			session.delete(province);
			session.flush();
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

	}

	public List<Province> queryByName(String pro_name) throws Exception {
		List<Province> list = new ArrayList<Province>();
		try {
			session = HibernateSession.getSession();
			String hql = "from Province province where province.pro_name=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, pro_name);
			list = q.list();

		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

		return list;

	}

	public void update(Province province) throws Exception {

		try {
			session = HibernateSession.getSession();
			Province province_update = (Province) session.get(Province.class,
					new Integer(province.getId()));
			province_update.setPro_name(province.getPro_name());
			session.flush();
		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

	}

}
