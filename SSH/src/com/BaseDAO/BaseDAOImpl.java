package com.BaseDAO;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.SessionFactory.HibernateSession;

public class BaseDAOImpl<T> implements BaseDAO<T> {
	
	private Class<T> clazz;
	private Session session=null;
	
	@SuppressWarnings("unchecked")
	public BaseDAOImpl(){
		clazz=(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	public void delete(int id) throws Exception {
		try {
			session = HibernateSession.getSession();
			T entity = (T) session.get(clazz, new Integer(id));
			session.delete(entity);
			session.flush();
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}	
	}

	public void insert(T entity) throws Exception {
		try {
			session = HibernateSession.getSession();
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print("数据插入失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<T> queryById(int id) throws Exception {
		List<T> list = new ArrayList<T>();
		try {
			session = HibernateSession.getSession();
			String hql = "from "+clazz.getName()+" entity where entity.id=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, id);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> query_all() throws Exception {
		List<T> list = new ArrayList<T>();
		try {
			session = HibernateSession.getSession();
			String hql =  "from "+clazz.getName();
			Query q = session.createQuery(hql);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}

		return list;
	}
	
	public void update(T entity) throws Exception {
		try {
			session = HibernateSession.getSession();
			session.update(entity);
			session.flush();
		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		} finally {
			HibernateSession.closeSession();
		}
		
	}
	
}
