package com.BaseDAO;

import java.util.List;

public interface BaseDAO<T> {

	public void insert(T entity) throws Exception;

	public List<T> queryById(int id) throws Exception;

	public void delete(int id) throws Exception;
	
	public List<T> query_all() throws Exception;
	
	public void update(T entity) throws Exception;

}
