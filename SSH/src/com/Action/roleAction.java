package com.Action;

import java.util.ArrayList;
import java.util.List;
import com.DAO.RoleDAO;
import com.VO.Role;
import com.opensymphony.xwork2.ActionSupport;

public class roleAction extends ActionSupport{
	private static final long serialVersionUID=1L;
	
	private List<Role> list_role=null;
	private String info,bar="��1��",rolename,asc="ASC";
	private int id,userid;
	
	private RoleDAO roleDAO;
	private Role role;

	public roleAction(){
		list_role=new ArrayList<Role>();
	}
	
	public String Query() throws Exception{
		
		if(rolename.equals("")){
			if(roleDAO.query_all().size()!=0){
				list_role=roleDAO.query_all();
				info="result_query_role";
			}else {
				info="result_query_fail_role";
			}
		}else if(roleDAO.queryByName(rolename).size()!=0){
			list_role=roleDAO.queryByName(rolename);
			info="result_query_role";
		}else {
			info="result_query_fail_role";
		}
		
		return SUCCESS;
	}
	
	public String Delete_role() throws Exception{
		
		roleDAO.delete_role(id);
		rolename="";
		Query();
		
		return SUCCESS;
	}
	public String Delete()throws Exception{
		
		roleDAO.delete(userid, id);
		Query();
			
		return SUCCESS;
	}
	
	public String Update()throws Exception{
		
		role.setId(id);
		role.setRolename(rolename);
		roleDAO.update(role);
	
		Query();
		
		return SUCCESS;
	}
	
	
	

	public RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public List<Role> getList_role() {
		return list_role;
	}
	public void setList_role(List<Role> listRole) {
		list_role = listRole;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getBar() {
		return bar;
	}
	public void setBar(String bar) {
		this.bar = bar;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAsc() {
		return asc;
	}
	public void setAsc(String asc) {
		this.asc = asc;
	}
	

}
