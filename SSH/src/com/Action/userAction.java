package com.Action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import com.opensymphony.xwork2.ActionSupport;
import com.DAO.*;
import com.VO.*;

public class userAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private String[] rolename;
	private int idcardCode, userid, page = 1;
	private String pro_name, username, password, info, bar, asc = "DESC";
	private List<User> list_user;
	// Resource resource=new ClassPathResource("applicationContext.xml");
	// BeanFactory factory =new XmlBeanFactory(resource);
	private User user;
	private Province province;
	private Idcard idcard;
	private ProvinceDAO proDAO;
	private RoleDAO roleDAO;
	private UserDAO userDAO;

	public userAction() {
		list_user = new ArrayList<User>();
		// user =(User)factory.getBean("user");
		// province=(Province)factory.getBean("province");
		// idcard=(Idcard)factory.getBean("idcard");
	}

	public String add() throws Exception {
		
		user.setPassword(password);
		user.setUsername(username);

		List<Province> list1 = proDAO.queryByName(pro_name);
		if (list1.size() != 0) {
			for (Province pro : list1) {
				user.setProvince(pro);
			}
		} else {
			province.setPro_name(pro_name);
			user.setProvince(province);
		}

		idcard.setUser(user);
		idcard.setIdcardCode(idcardCode);

		Role[] role = new Role[4];
		user.setRoles(new HashSet<Role>());
		for (int i = 0; i < rolename.length; i++) {
			role[i] = new Role();
			String rolenameValue = rolename[i];
			if (roleDAO.queryByName(rolenameValue).size() != 0) {
				Iterator<Role> it = roleDAO.queryByName(rolenameValue)
						.iterator();
				while (it.hasNext()) {
					role[i] = (Role) it.next();
				}
			} else {
				role[i].setRolename(rolenameValue);
				roleDAO.insert(role[i]);
			}
			user.getRoles().add(role[i]);
		}

		info = userDAO.insert(user, idcard);

		return SUCCESS;

	}

	public String delete() throws Exception {

		userDAO.delete(userid);
		username = "";
		query_name();

		return SUCCESS;
	}

	public String update() throws Exception {

		user.setUserid(userid);
		if (password != null)
			user.setPassword(password);
		if (pro_name != null) {
			province.setPro_name(pro_name);
			user.setProvince(province);
		}
		if (rolename != null) {
			Role[] role = new Role[4];
			user.setRoles(new HashSet<Role>());
			for (int i = 0; i < rolename.length; i++) {
				role[i] = new Role();
				String rolenameValue = rolename[i];
				if (roleDAO.queryByName(rolenameValue).size() != 0) {
					Iterator<Role> it = roleDAO.queryByName(rolenameValue)
							.iterator();
					while (it.hasNext()) {
						role[i] = (Role) it.next();
					}
				} else {
					role[i].setRolename(rolenameValue);
					roleDAO.insert(role[i]);
				}
				user.getRoles().add(role[i]);
			}
		}

		userDAO.update(user);

		query_id();

		return SUCCESS;
	}

	public String query_id() throws Exception {

		if (userDAO.queryById(userid).size()!=0) {
			list_user=userDAO.queryById(userid);
			info = "result_query_user";
		} else
			info = "result_query_fail_user";

		return SUCCESS;
	}

	public String query_name() throws Exception {

		if (username.equals("")) {
			if (userDAO.query_all().size() != 0) {
				list_user = userDAO.query_all();
				info = "result_query_user";
			} else {
				info = "result_query_fail_user";
			}
		} else if (userDAO.queryByName(username).size() != 0) {
			list_user = userDAO.queryByName(username);
			info = "result_query_user";
		} else {
			info = "result_query_fail_user";
		}

		return SUCCESS;
	}

	public String query_all() throws Exception { // ��ҳ����
		if (userDAO.find(page, asc).size() != 0) {
			list_user = userDAO.find(page, asc);
			bar = userDAO.bar(page, userDAO.findCount(),
					"userAction!query_all?asc=" + asc);
			info = "result_query_all";
		} else
			info = "result_query_*";

		return SUCCESS;
	}

	public ProvinceDAO getProDAO() {
		return proDAO;
	}

	public void setProDAO(ProvinceDAO proDAO) {
		this.proDAO = proDAO;
	}

	public RoleDAO getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(RoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Idcard getIdcard() {
		return idcard;
	}

	public void setIdcard(Idcard idcard) {
		this.idcard = idcard;
	}

	public String[] getRolename() {
		return rolename;
	}

	public void setRolename(String[] rolename) {
		this.rolename = rolename;
	}

	public List<User> getList_user() {
		return list_user;
	}

	public void setList_user(List<User> listUser) {
		list_user = listUser;
	}

	public int getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(int idcardCode) {
		this.idcardCode = idcardCode;
	}

	public String getAsc() {
		return asc;
	}

	public void setAsc(String asc) {
		this.asc = asc;
	}

	public String getBar() {
		return bar;
	}

	public String getPro_name() {
		return pro_name;
	}

	public void setPro_name(String proName) {
		pro_name = proName;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
