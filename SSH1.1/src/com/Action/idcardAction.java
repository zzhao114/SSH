package com.Action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.DAO.IdcardDAO;
import com.VO.Idcard;
import com.opensymphony.xwork2.ActionSupport;

public class idcardAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private int idcardCode, id;
	private List<Idcard> list_idcard = null;
	private String info, bar = "��1��";
	@Resource
	private IdcardDAO idcardDAO;
	@Resource
	private Idcard idcard;

	public idcardAction() {
		list_idcard = new ArrayList<Idcard>();
	}

	public String Query() throws Exception {

		String idcard = String.valueOf(idcardCode);
		if (idcard.equals("0")) {
			if (idcardDAO.query_all().size() != 0) {
				list_idcard = idcardDAO.query_all();
				info = "result_query_idcard";
			} else {
				info = "result_query_fail_idcard";
			}
		} else if (idcardDAO.queryByName(idcardCode).size() != 0) {
			list_idcard = idcardDAO.queryByName(idcardCode);
			info = "result_query_idcard";
		} else {
			info = "result_query_fail_idcard";
		}

		return SUCCESS;
	}

	public String Delete() throws Exception {

		idcardDAO.delete(id);

		Query();

		return SUCCESS;
	}

	public String Update() throws Exception {

		idcard.setId(id);
		idcard.setIdcardCode(idcardCode);
		idcardDAO.update(idcard);

		Query();

		return SUCCESS;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public List<Idcard> getList_idcard() {
		return list_idcard;
	}

	public void setList_idcard(List<Idcard> listIdcard) {
		list_idcard = listIdcard;
	}

	public int getIdcardCode() {
		return idcardCode;
	}

	public void setIdcardCode(int idcardCode) {
		this.idcardCode = idcardCode;
	}

}
