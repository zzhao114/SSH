package com.Action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.DAO.*;
import com.VO.Province;
import com.opensymphony.xwork2.ActionSupport;

public class provinceAction extends ActionSupport{
	private static final long serialVersionUID=1L;
	
	private List<Province> list_pro=null;
	private String info,bar="��1��",pro_name,asc="ASC";
	private int id;
	@Resource
	private Province province;
	@Resource
	private ProvinceDAO proDAO;
	@Resource
	private UserDAO userDAO;
	
	public provinceAction(){
		list_pro=new ArrayList<Province>();
	}
	
	public String Query() throws Exception {

		if(pro_name.equals("")){
			if(proDAO.query_all().size()!=0){
				list_pro=proDAO.query_all();
				info="result_query_pro";
			}else {
				info="result_query_fail_pro";
			}
		}else if(proDAO.queryByName(pro_name).size()!=0){
			list_pro=proDAO.queryByName(pro_name);
			info="result_query_pro";
		}else {
			info="result_query_fail_pro";
		}
			
		return SUCCESS;
	}

	public String Delete()throws Exception {
		
		proDAO.delete(id);
		pro_name="";
		Query();
		
		return SUCCESS;
	}
	
	public String Delete_user() throws Exception{
		
		userDAO.delete(id);
		Query();
		
		return SUCCESS;
	}
	
	public String Update()throws Exception{
		
		province.setId(id);
		province.setPro_name(pro_name);
		
		proDAO.update(province);
		
    	Query();
		
		return SUCCESS;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Province> getList_pro() {
		return list_pro;
	}

	public void setList_pro(List<Province> listPro) {
		list_pro = listPro;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getAsc() {
		return asc;
	}

	public void setAsc(String asc) {
		this.asc = asc;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public String getPro_name() {
		return pro_name;
	}

	public void setPro_name(String proName) {
		pro_name = proName;
	}
	
	
}
