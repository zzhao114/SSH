package com.VO;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("idcard")
@Scope("prototype")
public class Idcard {

     private int id;
     private int idcardCode;
     private User user;

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public Idcard() {
    }

    public Idcard(int idcardCode) {
        this.idcardCode = idcardCode;
    }

    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getIdcardCode() {
        return this.idcardCode;
    }
    
    public void setIdcardCode(int idcardCode) {
        this.idcardCode = idcardCode;
    }

}