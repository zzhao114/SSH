package com.BaseDAO;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class BaseDAOImpl<T> implements BaseDAO<T> {

	private Class<T> clazz;
	@Resource
	private SessionFactory sessionFactory;
	private Session session = null;

	@SuppressWarnings("unchecked")
	public BaseDAOImpl() {
		clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	public void delete(int id) throws Exception {
		try {
			session = sessionFactory.getCurrentSession();
			T entity = (T) session.get(clazz, new Integer(id));
			session.delete(entity);

		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		}
	}

	public void insert(T entity) throws Exception {
		try {
			session = sessionFactory.getCurrentSession();
			session.save(entity);

		} catch (Exception e) {
			System.out.print("数据插入失败！");
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public List<T> queryById(int id) throws Exception {
		List<T> list = new ArrayList<T>();
		try {
			session = sessionFactory.getCurrentSession();
			String hql = "from " + clazz.getName() + " entity where entity.id=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, id);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> query_all() throws Exception {
		List<T> list = new ArrayList<T>();
		try {
			session = sessionFactory.getCurrentSession();
			String hql = "from " + clazz.getName();
			Query q = session.createQuery(hql);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		}

		return list;
	}

	public void update(T entity) throws Exception {
		try {
			session = sessionFactory.getCurrentSession();
			session.update(entity);
		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		}

	}

}
