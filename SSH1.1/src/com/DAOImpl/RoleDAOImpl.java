package com.DAOImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.BaseDAO.BaseDAOImpl;
import com.DAO.RoleDAO;
import com.VO.Role;
import com.VO.User;

@SuppressWarnings("unchecked")
@Repository("roleDAO")
@Transactional
public class RoleDAOImpl extends BaseDAOImpl<Role> implements RoleDAO {
	
	@Resource
	private SessionFactory sessionFactory;
	Session session = null;

	public void delete(int userid, int roleid) throws Exception {// 删除用户权限
		try {
			session = sessionFactory.getCurrentSession();
			User user = (User) session.get(User.class, new Integer(userid));
			Role role = (Role) session.get(Role.class, new Integer(roleid));
			user.setRoles(new HashSet<Role>());
			user.getRoles().remove(role);
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		}
	}

	public void delete_role(int id) throws Exception {// 删除权限

		try {
			session = sessionFactory.getCurrentSession();
			Role role = (Role) session.get(Role.class, new Integer(id));
			session.delete(role);

		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		}

	}

	public List<Role> queryByName(String rolename) throws Exception {
		List<Role> list = new ArrayList<Role>();
		try {

			Session session = sessionFactory.getCurrentSession();
			String hql = "from Role role where role.rolename=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, rolename);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		}

		return list;
	}

	public void update(Role role) throws Exception {
		try {
			session = sessionFactory.getCurrentSession();
			Role role_update = (Role) session.get(Role.class, new Integer(role.getId()));

			role_update.setRolename(role.getRolename());

		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		}

	}

}
