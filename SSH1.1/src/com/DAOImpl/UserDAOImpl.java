package com.DAOImpl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.BaseDAO.BaseDAOImpl;
import com.VO.*;
import com.DAO.UserDAO;

@SuppressWarnings("unchecked")
@Repository("userDAO")
@Transactional
public class UserDAOImpl extends BaseDAOImpl<User> implements UserDAO {
	
	@Resource
	private SessionFactory sessionFactory;
	Session session = null;

	public void delete(int id) throws Exception {

		try {
			session = sessionFactory.getCurrentSession();
			User user = (User) session.get(User.class, new Integer(id));
			if (user.getProvince() != null) {
				user.getProvince().getUsers().remove(user);
				user.setProvince(null);
			}
			if (user.getRoles() != null) {
				Set<Role> roles = user.getRoles();
				for (Role role : roles) {
					user.setRoles(new HashSet<Role>());
					user.getRoles().remove(role);
					role.getUsers().remove(user);
				}
			}
			session.delete(user);
		
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		}

	}

	public String insert(User user, Idcard idcard) throws Exception {
		String info = "";
		try {
			session = sessionFactory.getCurrentSession();
			session.save(idcard);
			session.save(user);
			info = "result_add";
		} catch (Exception e) {
			info = "result_add_fail";
			session.getTransaction().rollback();
			System.out.print("数据添加失败！");
			e.printStackTrace();
		} 
		return info;
	}

	public List<User> queryById(int id) throws Exception {
		List<User> list = new ArrayList<User>();
		User user = null;
		try {
			session = sessionFactory.getCurrentSession();
			user = (User) session.get(User.class, new Integer(id));
			list.add(user);
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} 
		return list;

	}

	public List<User> queryByName(String username) throws Exception {

		List<User> list = new ArrayList<User>();
		list = null;
		try {
			session =sessionFactory.getCurrentSession();
			String hql = "from User u where u.username=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, username);
			list = q.list();
		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		} 

		return list;

	}

	public void update(User user) throws Exception {

		try {
			session = sessionFactory.getCurrentSession();
			User user_update = (User) session.get(User.class, new Integer(user
					.getUserid()));

			if (user.getpassword() != null)
				user_update.setPassword(user.getpassword());
			if (user.getProvince() != null) {
				Query q = session
						.createQuery("from Province province where province.pro_name=?");
				q.setParameter(0, user.getProvince().getPro_name());
				Iterator<Province> it = q.iterate();
				if (it.hasNext()) {
					user_update.setProvince((Province) it.next());
				} else {
					user_update.setProvince(user.getProvince());
				}
			}
			if (user.getRoles() != null)
				user_update.setRoles(user.getRoles());
			
		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		}

	}

	public List<User> find(int page, String asc) throws Exception {

		List<User> list = null;
		try {
			session = sessionFactory.getCurrentSession();
			String hql = "from User u order by u.userid " + asc;
			Query q = session.createQuery(hql);
			q.setFirstResult((page - 1) * User.PAGE_SIZE);
			q.setMaxResults(User.PAGE_SIZE);
			list = q.list();
		} catch (Exception e) {
			System.out.print("分页查询失败！");
			e.printStackTrace();
		} 
		return list;
	}

	public int findCount() throws Exception {
		int count = 0;
		try {
			session = sessionFactory.getCurrentSession();
			String hql = "from User u";
			Query q = session.createQuery(hql);
			List<User> list = q.list();
			count = list.size();
		} catch (Exception e) {
			System.out.print("数据总数查询失败！");
		}
		return count;
	}

	public String bar(int page, int count, String action) throws Exception {
		int currPage = 1;
		if (page != 0) {
			currPage = page;
		}
		int pages;
		if (count % User.PAGE_SIZE == 0) {
			pages = count / User.PAGE_SIZE;
		} else {
			pages = count / User.PAGE_SIZE + 1;
		}
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i <= pages; i++) {
			if (i == currPage) {
				sb.append("『" + i + "』");
			} else {
				sb.append("<a href='" + action + "&page=" + i + "'>" + i
						+ "</a>");
			}
			sb.append("　");
		}
		String bar = sb.toString();

		return bar;
	}

}
