package com.DAOImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.BaseDAO.BaseDAOImpl;
import com.DAO.ProvinceDAO;
import com.VO.Province;
import com.VO.User;

@SuppressWarnings("unchecked")
@Repository("proDAO")
@Transactional
public class ProvinceDAOImpl extends BaseDAOImpl<Province> implements ProvinceDAO {

	@Resource
	private SessionFactory sessionFactory;
	Session session = null;

	public void delete(int id) throws Exception {
		try {
			session = sessionFactory.getCurrentSession();
			Province province = (Province) session.get(Province.class, new Integer(id));
			Set<User> users = province.getUsers();
			if (province.getUsers() != null) {
				for (User user : users) {
					user.getProvince().setUsers(new HashSet<User>());
					user.setProvince(null);
					province.getUsers().remove(user);
				}
			}
			session.delete(province);
		} catch (Exception e) {
			System.out.print("数据删除失败！");
			e.printStackTrace();
		}

	}

	public List<Province> queryByName(String pro_name) throws Exception {
		List<Province> list = new ArrayList<Province>();
		try {
			session = sessionFactory.getCurrentSession();
			String hql = "from Province province where province.pro_name=?";
			Query q = session.createQuery(hql);
			q.setParameter(0, pro_name);
			list = q.list();

		} catch (Exception e) {
			System.out.print("数据查询失败！");
			e.printStackTrace();
		}

		return list;

	}

	public void update(Province province) throws Exception {

		try {
			session = sessionFactory.getCurrentSession();
			Province province_update = (Province) session.get(Province.class, new Integer(province.getId()));
			province_update.setPro_name(province.getPro_name());

		} catch (Exception e) {
			System.out.print("数据更新失败！");
			e.printStackTrace();
		}

	}

}
