#SSH

### 1.1 更新

将Spring和hibernate整合，由Spring管理事务提交和session开闭，使用注释的方法注入实例

### 模拟登入与查询，实现多表联级操作。

1. 以User表为主表，与Province表多对一双向关联
2. 以User表为主表，与Role表多对多双向关联
3. 以User表为主表，与Idcard表一对一双向关联
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/132434_c6a904b0_1191891.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/133048_81965769_1191891.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/132749_bfd2cb75_1191891.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/132953_aa0364e0_1191891.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/132510_cbe6f5a9_1191891.jpeg "在这里输入图片标题")

实现分页查询和升降序查询
![输入图片说明](http://git.oschina.net/uploads/images/2017/0123/132502_eeaf7575_1191891.jpeg "在这里输入图片标题")